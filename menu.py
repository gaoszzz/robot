from PyQt5.Qt import QWidget, QColor, QPixmap, QIcon, QSize, QCheckBox,QGridLayout,QListWidget
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QPushButton, QSplitter,\
    QComboBox, QLabel, QSpinBox, QFileDialog
from PyQt5.Qt import QPixmap, QPainter, QPoint, QPaintEvent, QMouseEvent, QPen,\
    QColor, QSize
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QTimer
from PyQt5.QtCore import Qt
import tensorflow as tf
import numpy as np
from PyQt5.QtWidgets import QApplication,QTextBrowser
try:
    import tensorflow.python.keras as keras
except:
    import tensorflow.keras as keras
from serialCommunication import *
import serial.tools.list_ports
import time
from robotControl import *
from kinematics import *
from link_vrep import *
import sys

class MainWidget(QWidget):


    def __init__(self, Parent=None):
        '''
        Constructor
        '''
        self.unfind0=0
        self.find0=0
        self.port = None 
        self.findport = None
        self.bps = 115200
        self.time_out=0.5
        self.rc=None
        super().__init__(Parent)
        self.__InitView()
        #self.InitUi()
                                                 # 串口初始化为None   
        self.timer = QTimer(self)                #实例化一个定时器  
        self.timer.timeout.connect(self.find) #定时器结束后触发refresh
        self.timer.start(5000)                   # 开启定时器，间隔0.5s
        self.find

        
    

        
    def __InitView(self):
        '''
                  初始化界面
        '''
        #新建一个水平布局作为本窗体的主布局
        main_layout = QHBoxLayout(self) 
        #设置主布局内边距以及控件间距为10px
        main_layout.setSpacing(10) 
        # 实例化QComBox对象
        

        

    
        
        
        #新建垂直子布局用于放置按键
        sub_layout = QVBoxLayout() 
        
        #设置此子布局和内部控件的间距为5px
        sub_layout.setContentsMargins(5, 5, 5, 5)
        
        splitter = QSplitter(self) #占位符
        sub_layout.addWidget(splitter)

        #self.text_browser = QTextBrowser(self)
        #self.text_browser.Move(10,280)
        #self.text_browser.resize(100,40)
       
        self.ca = QComboBox(self)
        #self.ca.move(100, 20)
        xLabel = QLabel('串口号',self)
        self.ca.addItem('None')
        self.ca.currentIndexChanged[str].connect(self.port_value) # 条目发生改变，发射信号，传递条目内容
        #self.cb.currentIndexChanged[int].connect(self.port_value)  # 条目发生改变，发射信号，传递条目索引
        sub_layout.addWidget(xLabel)
        sub_layout.addWidget(self.ca)
       
        # 多个添加条目
        self.cb = QComboBox(self)
        #self.cb.move(100, 20)
        yLabel = QLabel('波特率',self)
        self.cb.addItems(['115200', '9600'])

        # 信号
        self.cb.currentIndexChanged[str].connect(self.bps_value) # 条目发生改变，发射信号，传递条目内容
        #self.cb.currentIndexChanged[int].connect(self.bps_value)  # 条目发生改变，发射信号，传递条目索引
        sub_layout.addWidget(yLabel)
        sub_layout.addWidget(self.cb)



        self.__btn_Link=QPushButton("串口连接")
        self.__btn_Link.setParent(self)
        self.__btn_Link.clicked.connect(self.link_robot)
        sub_layout.addWidget(self.__btn_Link)

        self.__btn_Close = QPushButton("断开连接")
        self.__btn_Close.setParent(self)
        self.__btn_Close.clicked.connect(self.link_close)
        sub_layout.addWidget(self.__btn_Close)

        self.listFile = QListWidget()
        self.__btn_Refresh = QPushButton("刷新")
        self.__btn_Refresh.setParent(self) #设置父对象为本界面
     
       
    
        refresh = QGridLayout(self)
        sub_layout.addWidget(self.listFile)
        sub_layout.addWidget(self.__btn_Refresh)
        self.__btn_Refresh.clicked.connect(self.Refresh)
        self.setLayout(refresh)


        self.__btn_weizhi=QPushButton("到达目标位置")
        self.__btn_weizhi.setParent(self)
        self.__btn_weizhi.clicked.connect(self.weizhi)
        sub_layout.addWidget(self.__btn_weizhi)

        self.__btn_weizhi=QPushButton("垂直移动")
        self.__btn_weizhi.setParent(self)
        self.__btn_weizhi.clicked.connect(self.Move)
        sub_layout.addWidget(self.__btn_weizhi)

        self.__btn_Hand_control = QPushButton("手势控制")
        self.__btn_Hand_control.setParent(self) #设置父对象为本界面
        self.__btn_Hand_control.clicked.connect(self.hand_control)
        sub_layout.addWidget(self.__btn_Hand_control)

        self.__btn_simulation=QPushButton("离线仿真")
        self.__btn_simulation.setParent(self)
        self.__btn_simulation.clicked.connect(self.simulation)
        sub_layout.addWidget(self.__btn_simulation)
        
        self.__btn_Quit = QPushButton("退出")
        self.__btn_Quit.setParent(self) #设置父对象为本界面
        self.__btn_Quit.clicked.connect(self.Quit)
        sub_layout.addWidget(self.__btn_Quit)
        
       
        main_layout.addLayout(sub_layout) #将子布局加入主布局

    def port_value(self, i):
        self.port=i
        
    
    def bps_value(self, i):
        self.bps=int(i)



    
    def link_close(self):
        global Link
        # print(self.main_engine.is_open)  # 检验串口是否打开
        # 判断是否打开
        if (Link==False):
            self.listFile.addItem("未连接")
        else:
            if(self.bps==9600):
                self.comm1.main_engine.close()
            else:
                self.comm.main_engine.close()
            Link=False
            self.listFile.addItem("已断开")
            #self.timer.start(3000)

      
        

            
    def link_robot(self):
        global Link
        if(Link==True):
            self.listFile.addItem("已连接")
        else:
            if(self.bps==9600):
                self.comm1 = Communication(self.port,self.bps,timeout=self.time_out,stopbit=1,bytesize=8)
                self.rc = robotControl(self.comm1)
            else:
                self.comm = Communication(self.port,self.bps,timeout=self.time_out,stopbit=None,bytesize=None,flag=1)
                self.rc = robotControl(self.comm)

            if(self.port!=None):
                Link=True
                self.listFile.addItem("连接成功")
                #self.timer.stop()
            else:
                self.listFile.addItem("未找到串口")
    """
    def InitUi(self):
        
        #窗口上的内容和窗口设置
        
        
   
        self.move(1,1)  # 移动到屏幕的左上角
        self.mylabel = QLabel(self).setText("串口号/波特率") # 创建一个QLabel并写入文字
    """
    def Refresh(self):
        self.listFile.clear()
        #self.unfind0=0
        #self.find0=0
        
        self.listFile.addItem("串口选择为%s"%(self.port))
        self.listFile.addItem("波特率选择为%d"%(self.bps))
        
        


    def find(self):
        global Link
        ports = list(serial.tools.list_ports.comports())
        self.findport=None
        k=0
        for p in ports:
            self.findport=p.name
            k+=1

            #self.ca.addItem(p.name)
        if(self.findport==None):
            if(self.unfind0==0):
                self.ca.clear()
                self.listFile.addItem("未检测到串口")
                self.ca.addItem('None')
                self.unfind0=1
                self.find0=0
            Link=False
            QApplication.processEvents()
            time.sleep(1)
        else:
            if(self.find0==0 or self.find0 !=k):
                self.ca.clear()
                i=0
                for p in ports:
                    self.listFile.addItem("找到串口"+p.name)
                    self.ca.addItem(p.name)
                    i+=1
                self.find0=i
                self.unfind0=0
            QApplication.processEvents()
            time.sleep(1)
            
    def simulation(self):
        from offline import QDemo
        if(Link==True):
            if(self.bps==9600): 
                print("test")
                self.w3 = QDemo(self.comm1)
                self.w3.show()                 
            else:
                self.listFile.addItem("请改波特率")
        else:
            self.listFile.addItem("请先连接串口")
    
        
    def hand_control(self):
        if(Link==True):
            if(self.bps==115200): 
                print("video")
                self.listFile.addItem("video")
                rc = robotControl(self.comm)
                rc.hand_control()
            else:
                self.listFile.addItem("请改波特率")
        else:
            self.listFile.addItem("请先连接串口")

            

    def weizhi(self):
        from link import QLabelBuddy
        if(Link==True):
            if(self.bps==115200):         
        # 做好其他窗口后先import进来后就简单调用就ok了
                #rc = robotControl(self.comm)
                self.w2 = QLabelBuddy(self.rc)
                self.w2.show()
            else:
                self.listFile.addItem("请改波特率")
        else:
            self.listFile.addItem("请先连接串口")

    def Move(self):
        from slider import QSliderDemo
        if(Link==True):         
        # 做好其他窗口后先import进来后就简单调用就ok了
            if(self.bps==115200):
                #rc = robotControl(self.comm)
                self.w1 = QSliderDemo(self.rc)
                self.w1.show()
            else:
                self.listFile.addItem("请改波特率")
        else:
            self.listFile.addItem("请先连接串口")
    
        
    def Quit(self):
        self.close()
    

   



import sys

def main():
    app = QApplication(sys.argv) 
    
    mainWidget = MainWidget() #新建一个主界面
    mainWidget.show()    #显示主界面
    exit(app.exec_()) #进入消息循环


    
if __name__ == '__main__':
    #port=link() 
    Link=False
            
    main()

