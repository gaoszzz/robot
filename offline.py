from PyQt5.QtWidgets import *
from robotControl import *
from link_vrep import *
import keyboard
import sys
class QDemo(QDialog) :
    def __init__(self,rc):
        super().__init__()
        self.rc=rc
        self.initUI()
        
        
        
        
    def initUI(self):
        self.resize(400,4)
        self.setWindowTitle('离线仿真')

        btnok = QPushButton('开始(长按q退出)')#实例一个按钮控件对象
        btnCancel = QPushButton('&关闭')#实例一个按钮控件对象
        mainLayout = QGridLayout(self)#实例一个网格布局器并指定父函数
        mainLayout.addWidget(btnok)
        btnok.clicked.connect(self.ok)
        
        mainLayout.addWidget(btnCancel)
        btnCancel.clicked.connect(self.cancelClicked)

    def ok(self):
        vs = Vrep_simulation(self.rc)
        while True:
            if keyboard.is_pressed('q'): 
                break
            vs.drive_joints()
            if keyboard.is_pressed('q'): 
                break
        
    def cancelClicked(self):
        self.close()
    
if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = QDemo()
    main.show()
    sys.exit(app.exec_())