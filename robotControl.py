from serialCommunication import *
import kinematics
import numpy as np
from scipy.interpolate import CubicSpline
import cv2
import time
# from link import *
import math
from link_vrep import *
from hand_detection import HandDetector as hdm

class robotControl(object):
    def __init__(self,link):
        self.scom = link
        self.joints_at_present = [0,-np.pi/2,0,0,0]  #每次移动过后，记录当前的关节空间位置
        self.position_at_present = [0,0,0.266]  #每次移动过后，记录当前末端执行器的笛卡尔空间位置
        self.q_limits = [(-np.pi/2, np.pi/2), (-np.pi, 0), (-np.pi/2, np.pi/2), (-np.pi/2, np.pi/2), (-np.pi/2, np.pi/2)]

    def check_position_limits(self, position):
        # 检查位置是否超出范围,超出返回True
        x,y,z = position
        if math.sqrt(x**2+y**2+z**2)>0.286:
            return True
        return False

    #移动到指定位置
    #输入：笛卡尔空间位置
    def move_to_point(self,position,pitch = None):
        self.scom.Open_Engine()
        # if self.check_position_limits(position):
        #     print("超出范围！")
        #     return
        inv_joints,pitch = kinematics.ana_ik(position,pitch)
        print(pitch)
        # bestq = []
        # for q in inv_joints:
        #     if not self.check_joint_limits(q):
        #         inv_joints.pop(q)
        print('指定点的可达关节空间解为：',inv_joints)
        # minimum = 10
        # for q_best in inv_joints:
        #     val = (q_best[0]-self.joints_at_present[0])**2 +(q_best[1]-self.joints_at_present[1])**2 +(q_best[2]-self.joints_at_present[2])**2 +(q_best[3]-self.joints_at_present[3])**2
        #     if val<minimum:
        #         bestq = q_best
        # print('指定点的可达关节空间最优解为：',bestq )
        pwm = [(int(700 * (inv_joints[0] * 180 / np.pi) / 90 + 1580), int(730 * (inv_joints[0] * 180 / np.pi) / 90 + 1580))[
                   inv_joints[0] > 0],
               (int(1000 * (inv_joints[1] * 180 / np.pi + 90) / 90 + 1500),
                int(700 * (inv_joints[1] * 180 / np.pi + 90) / 90 + 1500))[inv_joints[1] < -1.57],
               (int(-660 * (inv_joints[2] * 180 / np.pi) / 90 + 1540), int(-640 * (inv_joints[2] * 180 / np.pi) / 90 + 1540))[
                   inv_joints[2] > 0],
               (int(-810 * (inv_joints[3] * 180 / np.pi) / 90 + 1660), int(-590 * (inv_joints[3] * 180 / np.pi) / 90 + 1660))[
                   inv_joints[3] > 0]
               ]
        data=''
        for i in range(4):
            # if(pwm[i]<1000):
            #     data = f'#00{i}P0'+str(pwm[i])+'T1000!'
            data += f'#00{i}P'+'{0:0>{1}}'.format(pwm[i],4)+'T1000!'
        print(data)
        self.scom.Send_data('{'+data+'}')
        #位置改变，更新当前位置：
        
        self.position_at_present = kinematics.workspace(position)
        self.joints_at_present[:4] = inv_joints



    #移动到指定位置：
    #输入：笛卡尔空间位置
    def move_to_joints(self,joints):
        self.scom.Open_Engine()
        # inv_joints = kinematics.ana_ik(position)
        print('指定点的关节空间解为：',joints)
        pwm = [(int(700 * (joints[0] * 180 / np.pi) / 90 + 1580), int(730 * (joints[0] * 180 / np.pi) / 90 + 1580))[
                   joints[0] > 0],
               (int(1000 * (joints[1] * 180 / np.pi + 90) / 90 + 1500),
                int(700 * (joints[1] * 180 / np.pi + 90) / 90 + 1500))[joints[1] < -1.57],
               (int(-660 * (joints[2] * 180 / np.pi) / 90 + 1540), int(-640 * (joints[2] * 180 / np.pi) / 90 + 1540))[
                   joints[2] > 0],
               (int(-810 * (joints[3] * 180 / np.pi) / 90 + 1660), int(-590 * (joints[3] * 180 / np.pi) / 90 + 1660))[
                   joints[3] > 0]
               ]
        data =''
        for i in range(4):
            # if(pwm[i]<1000):
            #     data = f'#00{i}P0'+str(pwm[i])+'T1000!'
            # else:
            data += f'#00{i}P' + '{0:0>{1}}'.format(pwm[i], 4) + 'T1000!'
        print(data)
        joint_fk = [0]*5
        joint_fk[:4]=joints
        T50,_ = kinematics.fk(joint_fk)

        self.scom.Send_data('{'+data+'}')
        #更新当前笛卡尔空间与关节空间坐标
        self.joints_at_present[:4] = joints
        self.position_at_present = T50[:3,3]


    #机械臂抓取函数
    def capture(self):
        self.scom.Open_Engine()
        data_open_clamp = '#005P' + '2500' + 'T1000!'
        data_close_clamp = '#005P' + '0500' + 'T1000!'

        self.scom.Send_data(data_open_clamp)

    #路径规划
    def jtra_plan(self,position,num_points=3):
        joints = kinematics.jtra_plan(end_position=position,start_position=self.position_at_present,num_points=num_points)
        for i in range(num_points):
            print('现在移动到：',joints[i])
            self.move_to_joints(joints[i])
            sleep(1.5)


    def hand_control(self, fps_cap=60, show_fps=True, source=0):
        assert fps_cap >= 1, f"fps_cap should be at least 1\n"
        assert source >= 0, f"source needs to be greater or equal than 0\n"

        ctime = 0  # current time (used to compute FPS)
        ptime = 0  # past time (used to compute FPS)
        prev_time = 0  # previous time variable, used to set the FPS limit

        fps_lim = fps_cap  # FPS upper limit value, needed for estimating the time for each frame and increasing performances

        time_lim = 1. / fps_lim  # time window for each frame taken by the webcam

        # initialize hand and pose detector objects
        HandDet = hdm()

        cv2.setUseOptimized(True)  # enable OpenCV optimization

        # capture the input from the default system camera (camera number 0)
        cap = cv2.VideoCapture(source)
        if not cap.isOpened():  # if the camera can't be opened exit the program
            print("Cannot open camera")
            exit()
        aperture_old = 0
        dispersion_old = 0
        offset_old = 0
        angle_old = 0
        while True:  # infinite loop for webcam video capture

            # computed  delta time for FPS capping
            delta_time = time.perf_counter() - prev_time

            ret, frame = cap.read()  # read a frame from the webcam

            if not ret:  # if a frame can't be read, exit the program
                print("Can't receive frame from camera/stream end")
                break

            if delta_time >= time_lim:  # if the time passed is bigger or equal than the frame time, process the frame
                prev_time = time.perf_counter()

                # compute the actual frame rate per second (FPS) of the webcam video capture stream, and show it
                ctime = time.perf_counter()
                fps = 1.0 / float(ctime - ptime)
                ptime = ctime

                frame = HandDet.findHands(frame=frame, draw=True)
                hand_lmlist, frame = HandDet.findHandPosition(
                    frame=frame, hand_num=0, draw=False)

                if len(hand_lmlist) > 0:
                    frame, aperture_new = HandDet.findHandAperture(
                        frame=frame, verbose=True, show_aperture=True)
                    # J1_Pub.publish_once(message=3.0 - 0.3 * (aperture / 10))
                    # J2_Pub.publish_once(message=3.0 - 0.3 * (aperture / 10))

                    # -------------------------弯曲控制-------------------------------#
                    # frame = cv2.flip(frame,1)
                    # 控制机械臂弯曲
                    if np.abs(aperture_new - aperture_old) > 5:
                        if aperture_new > 90:
                            self.scom.Send_data("{" + "#003P" + "1300" + "T1000!"
                                                + "#002P" + "1500" + "T1000!"
                                                + "#001P" + "1500" + "T1000!" + "}")
                        elif 75 < aperture_new <= 90:
                            self.scom.Send_data("{" + "#003P" + "1300" + "T1000!"
                                                + "#002P" + "1400" + "T1000!"
                                                + "#001P" + "1500" + "T1000!" + "}")
                        elif 50 < aperture_new <= 75:
                            self.scom.Send_data("{" + "#003P" + "1100" + "T1000!"
                                                + "#002P" + "1300" + "T1000!" +
                                                "#001P" + "1500" + "T1000!" + "}")
                        elif 25 < aperture_new <= 50:
                            self.scom.Send_data("{" + "#003P" + "0900" + "T1000!"
                                                + "#002P" + "1100" + "T1000!" +
                                                "#001P" + "1400" + "T1000!" + "}")
                        else:
                            self.scom.Send_data(
                                "{" + "#003P" + "0900" + "T1000!"
                                + "#002P" + "1000" + "T1000!"
                                + "#001P" + "1100" + "T1000!" + "}")
                    aperture_old = aperture_new
                    # -------------------------弯曲控制-------------------------------#
                    # -------------------------夹取控制-------------------------------#
                    frame, dispersion_new = HandDet.findHandDispersion(
                        frame=frame, verbose=True, show_dispersion=True)
                    if np.abs(dispersion_new - dispersion_old) > 5:
                        if dispersion_new < 65:
                            self.scom.Send_data('#005P' + '2500' + 'T1000!')
                        elif 65 <= dispersion_new < 90:
                            self.scom.Send_data('#005P' + '1500' + 'T1000!')
                        else:
                            self.scom.Send_data('#005P' + '0500' + 'T1000!')
                    dispersion_old = dispersion_new
                    # -------------------------夹取控制-------------------------------#
                    # -------------------------控制旋转-------------------------------#
                    offset_new = HandDet.findHandOffset(frame=frame, verbose=True)
                    if np.abs(offset_new - offset_old) > 5:
                        PWM = (int(700 * (offset_new) / 60) + 1580, int(730 * (offset_new) / 60) + 1580)[
                            offset_new > 0]

                        self.scom.Send_data('#000P' + '{0:0>{1}}'.format(PWM, 4) + 'T1000!')
                    offset_old = offset_new
                    # --------------------------控制旋转-------------------------------#
                    # --------------------------夹具旋转-------------------------------#
                    frame, angle_new = HandDet.findHandAngle(frame=frame, verbose=True)

                    angle_new = abs(angle_new)
                    if np.abs(angle_new - angle_old) > 5:
                        pwm = ((int(530 * (angle_new - 90) / 90) + 1380, int(970 * (angle_new - 90) / 90) + 1380)[
                            angle_new < 90])
                        self.scom.Send_data('#004P' + '{0:0>{1}}'.format(pwm, 4) + 'T1000!')
                    angle_old = angle_new
                    # --------------------------夹具旋转-------------------------------#

                if show_fps:
                    cv2.putText(frame, "FPS:" + str(round(fps, 0)), (10, 400), cv2.FONT_HERSHEY_PLAIN, 2,
                                (255, 255, 255), 1)

                # show the frame on screen
                # vc = Vrep_simulation()
                #self.vc.drive_joints()
                cv2.imshow("Frame (press 'q' to exit)", frame)
            # if the key "q" is pressed on the keyboard, the program is terminated
            if cv2.waitKey(20) & 0xFF == ord('q'):
                break

        cap.release()
        cv2.destroyAllWindows()
        # killing the publishers after closing the camera window
        print("Killed Publisher Nodes")
   
    def straight_line(self,real_time_position,axis:str):
        # if self.check_position_limits(real_time_position):
        #     print("超出范围！")
        #     return
        # positions = np.squeeze([[np.array([self.position_at_present]) + (i+1)*(np.array([real_time_position])-np.array([self.position_at_present]))/4 for i in range(4)]])
        # print(positions)
        # joints_real = [[0]*5]*4
        # print(joints_real)
        # for i in range(4):
        #     print(positions[i])
        #     joints_real[i][:4] = kinematics.ana_ik(positions[i])
        #     self.move_to_point(position=positions[i])
        #     sleep(1)
        # print(joints_real)
        # # joints = kinematics.ana_ik(real_time_position)
        # # joints_real = [0]*5
        # # joints_real[:4] = joints
        # # self.move_to_point(position=real_time_position)
        # print(joints_real[3][:])
        ik = kinematics.ana_ik_forward(self.position_at_present,real_time_position,forward =axis)
        if not ik:
            print("超出范围！")
            return
        for i in range(len(ik)):
            print("现在移动到：",ik[i])
            self.move_to_joints(ik[i])
        self.position_at_present = real_time_position #joint里的更新可能不准确，这里用输入再更新一次
        return ik
    
    def straight_line1(self,real_time_position):
        if self.check_position_limits(real_time_position):
            print("超出范围！")
            return
        x1=real_time_position[0]-self.position_at_present[0]
        y1=real_time_position[1]-self.position_at_present[1]
        z1=real_time_position[2]-self.position_at_present[2]
        print(x1)
        print(y1)
        print(z1)
        if x1!=0:
            # a1=int(x1*1000)
            for i in range(3):

                self.position_at_present[0] += x1/3

                self.move_to_point(self.position_at_present )
                sleep(0.5)
                print(self.position_at_present)
        if y1!=0:
            # b1=int(y1*1000)
            # print(b1)
            for i in range(3):

                self.position_at_present[1] += y1/3

                self.move_to_point(self.position_at_present )
                sleep(0.5)
                print(self.position_at_present)
        if z1!=0:
            # c1=int(z1*1000)
            for i in range(3):

                self.position_at_present[2] += z1/3

                self.move_to_point(self.position_at_present )
                sleep(0.5)
                print(self.position_at_present)
        return self.position_at_present
    
    def print_sqare(self):
        sqare1 = [[0.08,0,0.08],[0.15,0,0.08],[0.15,0,0.16],[0.08,0,0.16],[0.08,0,0.08]]  #在y=0平面的正方形
        #sqare1 = [[0.08,0,0.08],[0.08,0,0.15],[0.15,0,0.15],[0.15,0,0.08],[0.08,0,0.08]]  #在y=0平面的正方形
        sqare2 = [[0.08,-0.07,0.08],[0.08,0.07,0.08],[0.08,0.07,0.15],[0.08,-0.07,0.15],[0.08,-0.07,0.08]]  #在x=0.08平面的正方形
        
        for j in range(5):
            if j%2 ==0 and j>0:
                self.straight_line1(sqare2[j]) #j为偶数时，在z轴上直线运动
                
            elif j%2!=0 and j>0:
                self.straight_line1(sqare2[j])
            else:
                self.move_to_point(sqare2[j])
            sleep(0.5)
        self.move_to_point(sqare1[0],pitch= 0)
        self.move_to_point(sqare1[0],pitch= 0)
        sleep(2)
        
        for i in range(5):
            if i%2 ==0 and i>0:
                self.straight_line1(sqare1[i],pitch= 0) #i为偶数时，在x轴上直线运动
                self.move_to_point(sqare1[i],pitch= 0)
            elif i%2!=0 and i>0:
                self.straight_line1(sqare1[i],pitch= 0)
                self.move_to_point(sqare1[i],pitch= 0)
            else:
                self.move_to_point(sqare1[i],pitch= 0)
                self.move_to_point(sqare1[i],pitch= 0)
            sleep(0.5)