import binascii
import os
import serial
from time import sleep
# serial = serial.Serial('COM3', 115200, timeout=0.5)

# 全局变量，串口是否创建成功标志
Ret = False
# 串口列表串口号
port_list_number = []
# 串口列表名称
port_list_name = []
class Communication(object):
    def __init__(self,com,bps,timeout,stopbit=None,bytesize=None,flag=0):
        self.port = com  # 串口号
        self.bps = bps  # 波特率
        self.timeout = timeout  # 超时溢出
        self.main_engine = None  # 全局串口通信对象
        global Ret
        Ret = False
        self.data = None
        self.b_c_text = None
        self.stopbit = stopbit
        self.bytesize = bytesize
        try:
            if(flag==0):
                self.main_engine = serial.Serial(self.port,self.bps,timeout=self.timeout,stopbits=self.stopbit,bytesize=self.bytesize)
            else:
                self.main_engine = serial.Serial(self.port,self.bps,timeout=self.timeout)
            if self.main_engine.isOpen():
                Ret = True
                print('连接成功！')
        except Exception as e:
            print("----打开串口异常----:")


    # 打开串口
    def Open_Engine(self):
        """
        打开串口
        """
        global Ret
        # 如果串口没有打开，则打开串口
        if not self.main_engine.isOpen():
            self.main_engine.open()
            Ret = True

    # 关闭串口
    def Close_Engine(self):
        """
        关闭串口
        """
        global Ret
        # print(self.main_engine.is_open)  # 检验串口是否打开
        # 判断是否打开
        if self.main_engine.isOpen():
            self.main_engine.close()  # 关闭串口
            Ret = False

    def Read_Size(self, size):
        """
        接收指定大小的数据
        :param size:
        :return:
        """
        return self.main_engine.read(size=size)

    # 接收一行数据
    # 使用readline()时应该注意：打开串口时应该指定超时，否则如果串口没有收到新行，则会一直等待。
    # 如果没有超时，readline会报异常。
    def Read_Line(self):
        """
        接收一行数据
        :return:
        """
        return self.main_engine.readline()

    # 发数据
    def Send_data(self, data):
        """
        发数据
        :param data:
        """
        self.main_engine.write(data.encode())
        print('已发送')
        sleep(0.1)

    def Send_hex_data(self,data):
        self.main_engine.write(data)
        print("已发送")
        sleep(0.1)




